      program main
c    
      use projection_engine
      use partition_engine
      use graphics_engine
c
      implicit none
c      
      integer(kind=4) :: nx, px, nelemx
      real   (kind=8), allocatable, dimension(:) :: Ux
      integer(kind=4) :: ny, py, nelemy
      real   (kind=8), allocatable, dimension(:) :: Uy
      integer(kind=4) :: nz, pz, nelemz
      real   (kind=8), allocatable, dimension(:) :: Uz
      real   (kind=8), allocatable, dimension(:) :: WEx
      real   (kind=8), allocatable, dimension(:) :: WEy
      real   (kind=8), allocatable, dimension(:) :: WEz

      double precision, allocatable, dimension(:,:) :: M
      double precision, allocatable, dimension(:) :: F
      
      integer(kind=4) :: resx,resy,resz,d
      real   (kind=8), allocatable, dimension(:,:) :: F3
      

      integer :: i
      integer :: iret
      integer :: iprint !debug flag
c
      iprint=1
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c     INPUT PARAMETERS
c  ...polynomial orders and numbers of intervals+1 in each direction 
c     e.g. nx=1 means 0 elements, nx=1 means 1 element
      px=2
      nx=50
      py=2
      ny=50
      pz=2
      nz=50
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c  ...prepare the problem dimensions
      allocate(Ux(nx+px+2)) !knot vector
      allocate(Uy(ny+py+2)) !knot vector
      allocate(Uz(nz+pz+2)) !knot vector
c fill the knot vector for x
      Ux(1:px+1)=0.d0
      Ux(nx+2:nx+px+2)=1.d0
      do i=px+2,nx+1
        Ux(i)=real(i-px-1)/real(nx-px+1)
      enddo
      nelemx = CountSpans(nx,px,Ux)
      if(iprint.eq.1)then
        write(*,*)'nx,px,nelemx',nx,px,nelemx
        write(*,*)'Ux',Ux
      endif
c prepare weights for edges for weighted bissections along x axis
      allocate(WEx(nelemx+1))
      call ComputeWeights(nx,px,nelemx,Ux,WEx)
c fill the knot vector for y
      Uy(1:py+1)=0.d0
      Uy(ny+2:ny+py+2)=1.d0
      do i=py+2,ny+1
        Uy(i)=real(i-py-1)/real(ny-py+1)
      enddo
      nelemy = CountSpans(ny,py,Uy)
      if(iprint.eq.1)then
        write(*,*)'ny,py,nelemy',ny,py,nelemy
        write(*,*)'Uy',Uy
      endif
c prepare weights for edges for weighted bissections along y axis
      allocate(WEy(nelemy+1))
      call ComputeWeights(ny,py,nelemy,Uy,WEy)
c fill the knot vector for z
      Uz(1:pz+1)=0.d0
      Uz(nz+2:nz+pz+2)=1.d0
      do i=pz+2,nz+1
        Uz(i)=real(i-pz-1)/real(nz-pz+1)
      enddo
      nelemz = CountSpans(nz,pz,Uz)
      if(iprint.eq.1)then
        write(*,*)'nz,pz,nelemz',nz,pz,nelemz
        write(*,*)'Uz',Uz
      endif
c prepare weights for edges for weighted bissections along z axis
      allocate(WEz(nelemz+1))
      call ComputeWeights(nz,pz,nelemz,Uz,WEz)
c
c  ...call recursive bissections
c
      if(iprint.eq.1)then
        write(*,*)'call construct_elimination_tree'
      endif
      call construct_elimination_tree
     . (Ux,px,nx,nelemx,Uy,py,ny,nelemy,Uz,pz,nz,nelemz,WEx,WEy,WEz)

      if(iprint.eq.1)then
        write(*,*)'call dumpout_nodes_and_tree'
      endif
      call dumpout_nodes_and_tree
     . (Ux,px,nx,nelemx,Uy,py,ny,nelemy,Uz,pz,nz,nelemz)

c
c  ...call mesh estimator
c
      if(iprint.eq.1)then
        write(*,*)'call flops_estimator'
      endif
      call SYSTEM ( " ./flops_estimator_f/analyser -f ./files/tree 
     .  > ./files/analysed " )
      
      allocate(F3((nz+1),(nx+1)*(ny+1))) !z,x,y

      if(iprint.eq.1)then
        write(*,*)'call readout_nodes_and_costs'
      endif
c ....read the dofs map      
      call readout_nodes_and_costs
     . (Ux,px,nx,nelemx,Uy,py,ny,nelemy,Uz,pz,nz,nelemz,F3)
c  ...call dimension dependent ParaView generator
c  ...graphics resolution 
      resx=1
      resy=1
      resz=1
      d=1 !number of components to visualize
      if(iprint.eq.1)then
        write(*,*)'call Graphics'
      endif
      call Graphics(1,d,
     .   Ux,px,nx,nelemx,resx,
     .   Uy,py,ny,nelemy,resy,
     .   Uz,pz,nz,nelemz,resz,
     .   F3,2)
c
      deallocate(F3)
      deallocate(Ux)
      deallocate(Uy)
      deallocate(Uz)
      deallocate(WEx)
      deallocate(WEy)
      deallocate(WEz)
c
      end

