      module partition_engine
      use elimination_tree_mod
      use projection_engine

      integer, allocatable, dimension(:) :: ELEM_NEW_ORDER
c ....our own METIS
      integer, allocatable, dimension(:) :: ELEM_OLD_ORDER
      double precision, allocatable, dimension(:) :: CENTER_ELEM
      integer, allocatable, dimension(:) :: INVERSE_MAP
      integer, dimension(:),allocatable :: buffer
c
      contains

      subroutine global2local(IGlobal,I_n,J_n,K_n,I,J,K)
      ig=IGlobal-1
      K = iabs(ig / ((I_n)*(J_n)))
      ig=ig-K*(I_n)*(J_n)
      J = ig/(I_n)
      ig=ig-(I_n)*J
      I=ig
c add 1 since local numbers also start from 1      
      I=I+1;J=J+1;K=K+1
      call local2global(I,J,K,I_n,J_n,K_n,IGlobalTest)
      if(IGlobal.ne.IGlobalTest)then
        write(*,*)'global->local: ERROR',IGlobal,IGlobalTest
        stop
      endif
      end subroutine global2local

      subroutine local2global(I,J,K,I_n,J_n,K_n,IGlobal)
      IGlobal = I + (J-1)*(I_n)+(K-1)*(I_n)*(J_n)
      end subroutine local2global
c
      subroutine neig_mdle(iel,nelemx,nelemy,nelemz,
     .                     WEx,WEy,WEz,
     .                     weights_mdle,nr_weights,
     .                     neigs_mdle,nr_neigs_mdle)
      integer :: mdle,nr_neigs_mdle,nr_weights_mdle
      integer :: neigs_mdle(27),weights_mdle(27)
      real   (kind=8), intent(in) :: WEx(1:nelemx)
      real   (kind=8), intent(in) :: WEy(1:nelemy)
      real   (kind=8), intent(in) :: WEz(1:max(1,nelemz))
      integer :: I,J,K, iprint
      call global2local(iel,nelemx,nelemy,nelemz,I,J,K)        
      nr_neigs_mdle=0; nr_weights=0
      if(.not.(I.eq.nelemx))then
         nr_neigs_mdle=nr_neigs_mdle+1
         nr_weights=nr_weights+1
         neigs_mdle(nr_neigs_mdle)=iel+1 !along X+
         weights_mdle(nr_weights)=WEx(I+1)
      endif
      if(.not.(I.eq.1))then
         nr_neigs_mdle=nr_neigs_mdle+1
         nr_weights=nr_weights+1
         neigs_mdle(nr_neigs_mdle)=iel-1 !along X-
         weights_mdle(nr_weights)=WEx(I)
      endif
      if(.not.(J.eq.nelemy))then
         nr_neigs_mdle=nr_neigs_mdle+1
         nr_weights=nr_weights+1
         neigs_mdle(nr_neigs_mdle)=iel+nelemx !along Y+
         weights_mdle(nr_weights)=WEy(J+1)
      endif
      if(.not.(J.eq.1))then
         nr_neigs_mdle=nr_neigs_mdle+1
         nr_weights=nr_weights+1
         neigs_mdle(nr_neigs_mdle)=iel-nelemx !along Y-
         weights_mdle(nr_weights)=WEy(J)
      endif
      if(nelemz.ne.0)then  
      if(.not.(K.eq.nelemz))then
         nr_neigs_mdle=nr_neigs_mdle+1
         nr_weights=nr_weights+1
         neigs_mdle(nr_neigs_mdle)=iel+nelemx*nelemy !along Z+
         weights_mdle(nr_weights)=WEz(K+1)
      endif
      if(.not.(K.eq.1))then
         nr_neigs_mdle=nr_neigs_mdle+1
         nr_weights=nr_weights+1
         neigs_mdle(nr_neigs_mdle)=iel-nelemx*nelemy !along Z-
         weights_mdle(nr_weights)=WEz(K)
      endif
      endif
      end subroutine neig_mdle
c
c--------------------------------------------------------------------------------------------
      subroutine construct_elimination_tree
     .    (Ux,px,nx,nelemx,Uy,py,ny,nelemy,Uz,pz,nz,nelemz,WEx,WEy,WEz)
c
c.....arguments
      integer :: ibeg, iend,ilevel, node_tree
      integer(kind=4), intent(in) :: nx, px, nelemx
      real   (kind=8), intent(in) :: Ux(0:nx+px+1)
      integer(kind=4), intent(in) :: ny, py, nelemy
      real   (kind=8), intent(in) :: Uy(0:ny+py+1)
      integer(kind=4), intent(in) :: nz, pz, nelemz
      real   (kind=8), intent(in) :: Uz(0:nz+pz+1)
      real   (kind=8), intent(in) :: WEx(1:nelemx)
      real   (kind=8), intent(in) :: WEy(1:nelemy)
      real   (kind=8), intent(in) :: WEz(1:max(nelemz,1))
c
      integer :: nelems
      double precision :: direction(3)
      integer, dimension(:),allocatable ::subdomains

      iprint = 0 !report elimination tree
c
c  create a list of all element indexes
      if(nelemz.eq.0)then
        allocate(ELEM_NEW_ORDER(nelemx*nelemy))
        iel=1
        do i=1,nelemx
           do j=1,nelemy
             ELEM_NEW_ORDER(iel)=iel
             iel=iel+1
           enddo
        enddo
        maxmdle = nelemx*nelemy
      else
        allocate(ELEM_NEW_ORDER(nelemx*nelemy*nelemz))
        iel=1  
        do i=1,nelemx
           do j=1,nelemy
             do k=1,nelemz
               ELEM_NEW_ORDER(iel)=iel
               iel=iel+1
             enddo
           enddo
        enddo
        maxmdle = nelemx*nelemy*nelemz
      endif
c create inverse map from mdle to iel
      allocate(INVERSE_MAP(maxmdle))
      INVERSE_MAP(1:maxmdle)=0
      do iel=1,maxmdle
         mdle=ELEM_NEW_ORDER(iel)
         INVERSE_MAP(mdle)=iel
      enddo
c  ...calling the routine construction the tree with the root node
c     each node has lists of all its elements (we call them subdomains)
      nr_subdomains = maxmdle 
      allocate(subdomains(nr_subdomains))
      do isub=1,nr_subdomains
        subdomains(isub)=isub-1 !subdomains start from 0
      enddo
      write(*,*)'construct elimination tree:',nr_subdomains
      call create_tree(nr_subdomains*2) 
      call create_root(subdomains,nr_subdomains)
      deallocate(subdomains)
c  ...calling the routine constructing the tree recursively - pass the root 
      direction(1:3) = 0.d0; direction(1) = 1.d0;
      nlevel=1
      if(nelemz==0)then
        nelems=nelemx*nelemy
      else
        nelems=nelemx*nelemy*nelemz
      endif
      call recursive_bisection_rec(direction,1,nelems,nlevel,
     .     TREE_INSTANCE%root,nelemx,nelemy,nelemz)
      if(iprint.eq.1)then
        call report_tree()
        call pause
      endif
      deallocate(INVERSE_MAP)
      deallocate(ELEM_NEW_ORDER)      
      end subroutine construct_elimination_tree
c--------------------------------------------------------------------
      recursive subroutine recursive_bisection_rec
     .   (direction,ibeg,iend,ilevel,node_tree,nelemx,nelemy,nelemz)
c
      double precision :: direction(3)
      double precision :: direction_here(3)
      integer :: ibeg,iend,ilevel,node_tree
      integer :: nelemx,nelemy,nelemz
c
c....local variables
      integer, allocatable, dimension(:) :: subdomains_left
      integer, allocatable, dimension(:) :: subdomains_right
      integer :: nr_subdomains_left, nr_subdomains_right
c
      iprint = 0
c
      if(iprint.eq.1)then
         write(*,*)'entering recursive_bisection:ibeg,iend:',ibeg,iend
         write(*,*)'ELEM_NEW_ORDER(ibeg,iend)',ELEM_NEW_ORDER(ibeg:iend)
         write(*,*)'direction:',direction
         call pause
      endif
c
      call sort_element_along_direction(direction,ibeg,iend,
     .  nelemx,nelemy,nelemz)
c      
      direction_here(1:3)=direction(1:3)
      call rotate_direction(direction_here,nelemz)
c
      ihalf = (iend+ibeg)/2
      if(iprint.eq.1)then
         write(*,*)'ibeg,iend,ihalf',ibeg,iend,ihalf
      endif
c - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
c     construct elimination tree
c - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
      if(ihalf.eq.ibeg)then
c special case, e.g. ibeg=1, ihalf=1, iend=2
        nr_subdomains_left = 1
        allocate(subdomains_left(nr_subdomains_left))
        subdomains_left(1)=ELEM_NEW_ORDER(ibeg)-1
        call create_new_son
     .    (node_tree,nr_subdomains_left,subdomains_left)
        deallocate(subdomains_left)
c
        nr_subdomains_right = iend-ihalf
        allocate(subdomains_right(nr_subdomains_right))
        do i=1,nr_subdomains_right
          subdomains_right(i)=ELEM_NEW_ORDER(ihalf+i)-1 
        enddo
        call create_new_son
     .    (node_tree,nr_subdomains_right,subdomains_right)
        deallocate(subdomains_right)
        ison1 = TREE_INSTANCE%TREE_NODES(node_tree)%sons(1)
        ison2 = TREE_INSTANCE%TREE_NODES(node_tree)%sons(2)
      else
        nr_subdomains_left = ihalf-ibeg+1
        allocate(subdomains_left(nr_subdomains_left))
        do i=1,nr_subdomains_left
          subdomains_left(i)=ELEM_NEW_ORDER(ibeg+i-1)-1
        enddo
        call create_new_son
     .    (node_tree,nr_subdomains_left,subdomains_left)
        deallocate(subdomains_left)
        nr_subdomains_right = iend-ihalf
        allocate(subdomains_right(nr_subdomains_right))
        do i=1,nr_subdomains_right
          subdomains_right(i)=ELEM_NEW_ORDER((ihalf+1)+i-1)-1
        enddo
        call create_new_son
     .    (node_tree,nr_subdomains_right,subdomains_right)
        deallocate(subdomains_right)
        ison1 = TREE_INSTANCE%TREE_NODES(node_tree)%sons(1)
        ison2 = TREE_INSTANCE%TREE_NODES(node_tree)%sons(2)
      endif
      ilevel=ilevel+1
c - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
c     3 or more subdomains in the first half
c - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
      if(ihalf-ibeg.gt.1)then
c  .....set elements to take part in the partition
        nbeg=ibeg; nhalf=ihalf; nlevel=ilevel
         call recursive_bisection_rec(direction_here,nbeg,nhalf,nlevel,
     .     ison1,nelemx,nelemy,nelemz)
      endif
c - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
c     2 subdomains in the first half
c - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
      if(ihalf-ibeg.eq.1)then
        nr_subdomains_left = 1
        allocate(subdomains_left(nr_subdomains_left))
        subdomains_left(1)=ELEM_NEW_ORDER(ibeg)-1
        call create_new_son
     .    (ison1,nr_subdomains_left,subdomains_left)
        deallocate(subdomains_left)
c
        nr_subdomains_right = 1
        allocate(subdomains_right(nr_subdomains_right))
        subdomains_right(1)=ELEM_NEW_ORDER(ihalf)-1
        call create_new_son
     .    (ison1,nr_subdomains_right,subdomains_right)
        deallocate(subdomains_right)
      endif
c - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
c     3 or more subdomains in the second half
c - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
      if(iend-(ihalf+1).gt.1)then
c  .....set elements to take part in the recursive partition
        nhalf=ihalf+1; nend=iend; nlevel=ilevel
         call recursive_bisection_rec(direction_here,nhalf,nend,nlevel,
     .     ison2,nelemx,nelemy,nelemz)
      endif
c - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
c     2 subdomains in the second half
c - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
      if(iend-(ihalf+1).eq.1)then
        nr_subdomains_left = 1
        allocate(subdomains_left(nr_subdomains_left))
        subdomains_left(1)=ELEM_NEW_ORDER(ihalf+1)-1 !since subdomains are counted from 0
        call create_new_son
     .    (ison2,nr_subdomains_left,subdomains_left)
        deallocate(subdomains_left)
c
        nr_subdomains_right = 1
        allocate(subdomains_right(nr_subdomains_right))
        subdomains_right(1)=ELEM_NEW_ORDER(iend)-1 !since subdomains are counted from 0
        call create_new_son
     .    (ison2,nr_subdomains_right,subdomains_right)
        deallocate(subdomains_right)
      endif
c - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
      end subroutine recursive_bisection_rec
c--------------------------------------------------------------------
      subroutine rotate_direction(direction,nelemz)
c
      double precision :: direction(3)
c
c      write(*,*)'rotate_direction:',direction(1:3),'->'
      if(nelemz==0)then
        if(abs(direction(1)-1.d0).lt.1.d-10)then
          direction(1:3) = 0.d0; direction(2) = 1.d0;
        else if(abs(direction(2)-1.d0).lt.1.d-10)then
c          direction(1:3) = 0.d0; direction(3) = 1.d0;
c        else if(abs(direction(3)-1.d0).lt.1.d-10)then
          direction(1:3) = 0.d0; direction(1) = 1.d0;
        endif
      else
        if(abs(direction(1)-1.d0).lt.1.d-10)then
          direction(1:3) = 0.d0; direction(2) = 1.d0;
        else if(abs(direction(2)-1.d0).lt.1.d-10)then
          direction(1:3) = 0.d0; direction(3) = 1.d0;
        else if(abs(direction(3)-1.d0).lt.1.d-10)then
          direction(1:3) = 0.d0; direction(1) = 1.d0;
        endif
      endif
c      write(*,*)direction(1:3)
c
      end subroutine rotate_direction
c--------------------------------------------------------------------
      subroutine sort_element_along_direction(direction,ibeg,iend,
     .     nelemx,nelemy,nelemz)
c
      double precision :: direction(3)
      integer :: ibeg,iend,ilevel
      integer :: nelemx,nelemy,nelemz  
      double precision :: xc(3)
c
      iprint=0
c
      nelems = iend-ibeg+1
      allocate(ELEM_OLD_ORDER(nelems))
      allocate(CENTER_ELEM(nelems))
      allocate(buffer(nelems))
      buffer(1:nelems)=0
      ELEM_OLD_ORDER(1:nelems)=ELEM_NEW_ORDER(ibeg:iend)
c      buffer(1:nelems)=ELEM_NEW_ORDER(ibeg:iend) !MP
c
      mdle=0
      do iel=1,nelems
        mdle=ELEM_OLD_ORDER(iel)
        call global2local(mdle,nelemx,nelemy,nelemz,i,j,k)
        xc(1)=i*1.d0; xc(2)=j*1.d0; xc(3)=k*1.d0
        call scalar_product(xc,direction, CENTER_ELEM(iel))
      enddo
      if(iprint.eq.1)then
        write(*,*)'call sortm(nelems,buffer,CENTER_ELEM)'
        write(*,*)'nelems',nelems
        write(*,*)'buffer(1,nelems)',buffer(1:nelems)
        write(*,*)'direction',direction
        write(*,*)'CENTER_ELEM(1,nelems)',CENTER_ELEM(1:nelems)
      endif
      call sortm(nelems) !,buffer,CENTER_ELEM)
      if(iprint.eq.1)then
        write(*,*)'after call sortm(nelems,buffer,CENTER_ELEM)'
        write(*,*)'buffer(1,nelems)',buffer(1:nelems)
      endif
      do iel=1,nelems
        iel1=buffer(iel)
        ELEM_NEW_ORDER(ibeg+iel-1) = ELEM_OLD_ORDER(iel1)
      enddo
      if(iprint.eq.1)then
        write(*,*)'exiting sort_elem_along_direction'
        write(*,*)'ELEM_NEW_ORDER(ibeg,iend)',ELEM_NEW_ORDER(ibeg:iend)
      endif
      deallocate(ELEM_OLD_ORDER,CENTER_ELEM,buffer)           
c
      end subroutine sort_element_along_direction
c-----------------------------------------------------------------------
      subroutine dumpout_nodes_and_tree
     .   (Ux,px,nx,nelemx,Uy,py,ny,nelemy,Uz,pz,nz,nelemz)
      !!$ d is dimension
      !!$ U is the knot vector
      !!$ p is the polynomial order
      !!$ n is the index of the last control point
      !!$ nelem you get from running CountSpans
      implicit none
      integer(kind=4), intent(in) :: nx, px, nelemx
      real   (kind=8), intent(in) :: Ux(0:nx+px+1)
      integer(kind=4), intent(in) :: ny, py, nelemy
      real   (kind=8), intent(in) :: Uy(0:ny+py+1)
      integer(kind=4), intent(in) :: nz, pz, nelemz
      real   (kind=8), intent(in) :: Uz(0:nz+pz+1)
c
      integer(kind=4) :: mx,my,mz,ngx,ngy,ngz,ex,ey,ez
      integer(kind=4) :: ex1,ey1,ez1
      integer(kind=4) :: kx,ky,kz,ax,ay,az,d
      integer(kind=4) :: kx1,ky1,kz1,ax1,ay1,az1
      integer(kind=4) :: Ox(nelemx),Oy(nelemy),Oz(max(nelemz,1))
      real   (kind=8) :: Jx(nelemx),Jy(nelemy),Jz(max(nelemz,1))
      real   (kind=8) :: Wx(px+1),Wy(py+1),Wz(pz+1)
      real   (kind=8) :: Xx(px+1,nelemx)
      real   (kind=8) :: Xy(py+1,nelemy)
      real   (kind=8) :: Xz(pz+1,max(nelemz,1))
      real   (kind=8) :: NNx(0:0,0:px,px+1,nelemx),
     .                   NNy(0:0,0:py,py+1,nelemy),
     .                   NNz(0:0,0:pz,pz+1,max(nelemz,1))
      integer(kind=4) :: n, iel, idof, nelem_dofs, ind,  
     .                   iprint, i, nelem, ilayer
      integer(kind=4), allocatable, dimension(:) :: nelement_dofs

      iprint=0 ! for debugging the output
c
      mx  = nx+px+1
      ngx = px+1
      my  = ny+py+1
      ngy = py+1
      mz  = nz+pz+1
      ngz = pz+1
      d = 0
c
      call BasisData(px,mx,Ux,d,ngx,nelemx,Ox,Jx,Wx,Xx,NNx)
      call BasisData(py,my,Uy,d,ngy,nelemy,Oy,Jy,Wy,Xy,NNy)
      call BasisData(pz,mz,Uz,d,ngz,nelemz,Oz,Jz,Wz,Xz,NNz)
c
      open(unit=40,file='files/tree',
     .     form='formatted',access='sequential',status='unknown')

      if(iprint.eq.1)write(40,*)'number of nodes'       
      if(nelemz==0)then
        n = (nelemx+px)*(nelemy+py)
      else
        n = (nelemx+px)*(nelemy+py)*(nelemz+pz)
      endif
c
      write(40,*)n
c
      if(iprint.eq.1)write(40,*)'node id / number of dofs'
      do i=1,n
         write(40,*)i,1
      enddo

      if(iprint.eq.1)write(40,*)'Element number'
      if(nelemz==0)then
        nelem = nelemx*nelemy
      else
        nelem = nelemx*nelemy*nelemz
      endif
      write(40,*)nelem

c  ...no refinements, so all the elements are on the first layer
      do ilayer=1,1
        if(nelemz==0)then
c  .....loop through elements on layer 
        iel=0
        do ex = 1,nelemx
           do ey = 1,nelemy
                 iel=iel+1 !element count
                 nelem_dofs = (px+1)*(py+1)
                 idof=0
                 allocate(nelement_dofs(nelem_dofs))
                          do ax = 0,px
                             do ay = 0,py
                                   ind = 0
                                   ind = ind + (Oy(ey)+ay)*(nx+1)
                                   ind = ind + (Ox(ex)+ax)
                                   idof=idof+1
                                   nelement_dofs(idof)=ind+1 !MP +1
                             enddo
                          enddo
                 write(40,*)ilayer,iel,
     .             nelem_dofs,nelement_dofs(1:nelem_dofs)
                 deallocate(nelement_dofs)
           enddo
        enddo
        else
c  .....loop through elements on layer 
        iel=0
        do ex = 1,nelemx
           do ey = 1,nelemy
              do ez = 1,nelemz
                 iel=iel+1 !element count
                 nelem_dofs = (px+1)*(py+1)*(pz+1)
                 idof=0
                 allocate(nelement_dofs(nelem_dofs))
                          do ax = 0,px
                             do ay = 0,py
                                do az = 0,pz
c global index of dof
                                   ind = (Oz(ez)+az)*(ny+1)*(nx+1)
                                   ind = ind + (Oy(ey)+ay)*(nx+1)
                                   ind = ind + (Ox(ex)+ax)
                                   idof=idof+1
                                   nelement_dofs(idof)=ind+1 !MP +1
                                enddo
                             enddo
                          enddo
                 write(40,*)ilayer,iel,
     .             nelem_dofs,nelement_dofs(1:nelem_dofs)
                 deallocate(nelement_dofs)
              enddo
           enddo
        enddo
        endif !nelemz!=0
      enddo
      if(iprint.eq.1)write(40,*)'ELIMINATION TREE:'
      call dumpout_tree
      close(40)
      end subroutine dumpout_nodes_and_tree
      
      subroutine readout_nodes_and_costs
     .   (Ux,px,nx,nelemx,Uy,py,ny,nelemy,Uz,pz,nz,nelemz,F3)
      !!$ d is dimension
      !!$ U is the knot vector
      !!$ p is the polynomial order
      !!$ n is the index of the last control point
      !!$ nelem you get from running CountSpans
      implicit none
      integer(kind=4), intent(in) :: nx, px, nelemx
      real   (kind=8), intent(in) :: Ux(0:nx+px+1)
      integer(kind=4), intent(in) :: ny, py, nelemy
      real   (kind=8), intent(in) :: Uy(0:ny+py+1)
      integer(kind=4), intent(in) :: nz, pz, nelemz
      real   (kind=8), intent(in) :: Uz(0:nz+pz+1)
      real   (kind=8), intent(inout) :: 
     .            F3(1:max((nz+1),1),0:(nx+1)*(ny+1)-1)
c
      integer(kind=4) :: mx,my,mz,ngx,ngy,ngz,ex,ey,ez
      integer(kind=4) :: ex1,ey1,ez1
      integer(kind=4) :: kx,ky,kz,ax,ay,az,d
      integer(kind=4) :: kx1,ky1,kz1,ax1,ay1,az1
      integer(kind=4) :: Ox(nelemx),Oy(nelemy),Oz(max(1,nelemz))
      real   (kind=8) :: Jx(nelemx),Jy(nelemy),Jz(max(1,nelemz))
      real   (kind=8) :: Wx(px+1),Wy(py+1),Wz(pz+1)
      real   (kind=8) :: Xx(px+1,nelemx)
      real   (kind=8) :: Xy(py+1,nelemy)
      real   (kind=8) :: Xz(pz+1,max(1,nelemz))
      real   (kind=8) :: NNx(0:0,0:px,px+1,nelemx),
     .                   NNy(0:0,0:py,py+1,nelemy),
     .                   NNz(0:0,0:pz,pz+1,max(1,nelemz))
      integer(kind=4) :: n, iel, idof, nelem_dofs, ind,  
     .                   iprint, i, nelem, ilayer
      integer(kind=4), allocatable, dimension(:) :: nelement_dofs
      integer :: ndof,flop,loop
      character(1)    :: car
      real   (kind=8), allocatable, dimension(:) :: dofs_flops
      real   (kind=8), allocatable, dimension(:,:,:) :: V
      real   (kind=8), allocatable, dimension(:) :: temp
      integer(kind=4) :: ix,iy,iz

      iprint=0 ! for debugging the output
c
      mx  = nx+px+1
      ngx = px+1
      my  = ny+py+1
      ngy = py+1
      mz  = nz+pz+1
      ngz = pz+1
      d = 0
c
      call BasisData(px,mx,Ux,d,ngx,nelemx,Ox,Jx,Wx,Xx,NNx)
      call BasisData(py,my,Uy,d,ngy,nelemy,Oy,Jy,Wy,Xy,NNy)
      call BasisData(pz,mz,Uz,d,ngz,nelemz,Oz,Jz,Wz,Xz,NNz)
      
      open(unit=41,file='files/analysed',
     .     form='formatted',access='sequential',status='unknown')
      if(nelemz==0)then
        n = (nelemx+px)*(nelemy+py)
      else
        n = (nelemx+px)*(nelemy+py)*(nelemz+pz)
      endif
      if(iprint.eq.1)write(*,*)'number of nodes ' , n    
      
      allocate(dofs_flops(n))
      
      do loop=1,n
         read (41,*) ndof,car,flop

        dofs_flops(ndof+1) = flop
        if(iprint.eq.1) write(*,*) ndof,'=',flop
      end do
      
      if(iprint.eq.1) then
         do loop=1,n
            write(*,*) dofs_flops(loop)
         end do
      end if

      if(nelemz==0)then
      allocate (V(nx+1,ny+1,1))
      
      do ix=1,nx+1
         do iy=1,ny+1
               V(ix,iy,1) = dofs_flops(iy
     .             + (ix-1)*(ny+1))
         end do
      end do
      
      
      do ix=0,nx
         do iy=0,ny
               F3(1,(nx+1)*iy+ix) = V(ix+1,iy+1,1)
         end do
      end do
      else
      
      allocate (V(nx+1,ny+1,nz+1))
      
      do ix=1,nx+1
         do iy=1,ny+1
            do iz=1,nz+1
               V(ix,iy,iz) = dofs_flops(iz + (iy-1)*(nz+1)
     .             + (ix-1)*(nz+1)*(ny+1))
            end do
         end do
      end do
      
      
      do ix=0,nx
         do iy=0,ny
            do iz=0,nz
               F3(iz+1,(nx+1)*iy+ix) = V(ix+1,iy+1,iz+1)
            end do
         end do
      end do
      endif !nelemz!=0
      
      close (41)
      deallocate(dofs_flops)
      deallocate(V)
      end subroutine readout_nodes_and_costs

ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine sortm(Nrvistr)

      integer Nrvistr

c  ...initialize index array
      do 10 i=1,NRVISTR
        buffer(i) = i
   10 continue
      if (NRVISTR.eq.1) return

c
c  ...start sorting (heapsort algorithm)
      l=NRVISTR/2+1
      ir=NRVISTR
   20 continue 
      if (l.gt.1) then
        l=l-1
        index=buffer(l)
        q=CENTER_ELEM(index)
      else
        index=buffer(ir)
        q=CENTER_ELEM(index)
        buffer(ir)=buffer(1)
        ir = ir - 1
        if (ir.eq.1) then
          buffer(1) = index

ccccc     return
          go to 700

        endif
      endif
      i=l
      j=l*2
   30 continue
      if (j.gt.ir) go to 40

      if(j.lt.ir) then
        if(CENTER_ELEM(buffer(j)).lt.CENTER_ELEM(buffer(j+1))) j=j+1
      endif



      if (q.lt.CENTER_ELEM(buffer(j))) then
        buffer(i) = buffer(j)
        i=j
        j=j+j
      else
        j=ir+1
      endif
      go to 30
   40 continue
      buffer(i) = index
      go to 20
c

  700 continue

      end
              
      end module partition_engine
