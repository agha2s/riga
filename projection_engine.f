      module projection_engine

      contains

      subroutine GaussRule(q,X,W)
c order of the function being interpolated
c X coordinates
c W weights
      implicit none
      integer(kind=4), intent(in)  :: q
      real   (kind=8), intent(out) :: X(0:q-1)
      real   (kind=8), intent(out) :: W(0:q-1)
      select case (q)
      case (1) ! p = 1
      X(0) = 0.0
      W(0) = 2.0
      case (2) ! p = 3
      X(0) = -0.5773502691896257645091487805019576 ! 1/sqrt(3)
      X(1) = -X(0)
      W(0) =  1.0
      W(1) =  W(0)
      case (3) ! p = 5
      X(0) = -0.7745966692414833770358530799564799 ! sqrt(3/5)
      X(1) =  0.0
      X(2) = -X(0)
      W(0) =  0.5555555555555555555555555555555556 ! 5/9
      W(1) =  0.8888888888888888888888888888888889 ! 8/9
      W(2) =  W(0)
      case (4) ! p = 7
      X(0) = -0.8611363115940525752239464888928094 ! sqrt((3+2*sqrt(6/5))/7)
      X(1) = -0.3399810435848562648026657591032448 ! sqrt((3-2*sqrt(6/5))/7)
      X(2) = -X(1)
      X(3) = -X(0)
      W(0) =  0.3478548451374538573730639492219994 ! (18-sqrt(30))/36
      W(1) =  0.6521451548625461426269360507780006 ! (18+sqrt(30))/36
      W(2) =  W(1)
      W(3) =  W(0)
      case (5) ! p = 9
      X(0) = -0.9061798459386639927976268782993929 ! 1/3*sqrt(5+2*sqrt(10/7))
      X(1) = -0.5384693101056830910363144207002086 ! 1/3*sqrt(5-2*sqrt(10/7))
      X(2) =  0.0
      X(3) = -X(1)
      X(4) = -X(0)
      W(0) =  0.2369268850561890875142640407199173 ! (322-13*sqrt(70))/900
      W(1) =  0.4786286704993664680412915148356382 ! (322+13*sqrt(70))/900
      W(2) =  0.5688888888888888888888888888888889 ! 128/225
      W(3) =  W(1)
      W(4) =  W(0)
      case (6)
      X(0) = -0.9324695142031520
      X(1) = -0.6612093864662645
      X(2) = -0.2386191860831969
      X(3) = -X(2)
      X(4) = -X(1)
      X(5) = -X(0)
      W(0) =  0.1713244923791703
      W(1) =  0.3607615730481386
      W(2) =  0.4679139345726911
      W(3) =  W(2)
      W(4) =  W(1)
      W(5) =  W(0)
      case (7)
      X(0) = -0.9491079123427585
      X(1) = -0.7415311855993944
      X(2) = -0.4058451513773972
      X(3) =  0.0
      X(4) = -X(2)
      X(5) = -X(1)
      X(6) = -X(0)
      W(0) =  0.1294849661688697
      W(1) =  0.2797053914892767
      W(2) =  0.3818300505051189
      W(3) =  0.4179591836734694
      W(4) =  W(2)
      W(5) =  W(1)
      W(6) =  W(0)
      case (8)
      X(0) = -0.9602898564975362
      X(1) = -0.7966664774136267
      X(2) = -0.5255324099163290
      X(3) = -0.1834346424956498
      X(4) = -X(3)
      X(5) = -X(2)
      X(6) = -X(1)
      X(7) = -X(0)
      W(0) =  0.1012285362903763
      W(1) =  0.2223810344533745
      W(2) =  0.3137066458778873
      W(3) =  0.3626837833783620
      W(4) =  W(3)
      W(5) =  W(2)
      W(6) =  W(1)
      W(7) =  W(0)
      case (9)
      X(0) = -0.9681602395076261
      X(1) = -0.8360311073266358
      X(2) = -0.6133714327005904
      X(3) = -0.3242534234038089
      X(4) =  0.0
      X(5) = -X(3)
      X(6) = -X(2)
      X(7) = -X(1)
      X(8) = -X(0)
      W(0) =  0.0812743883615744
      W(1) =  0.1806481606948574
      W(2) =  0.2606106964029354
      W(3) =  0.3123470770400029
      W(4) =  0.3302393550012598
      W(5) =  W(3)
      W(6) =  W(2)
      W(7) =  W(1)
      W(8) =  W(0)
      case (10)
      X(0) = -0.973906528517172
      X(1) = -0.865063366688985
      X(2) = -0.679409568299024
      X(3) = -0.433395394129247
      X(4) = -0.148874338981631
      X(5) = -X(4)
      X(6) = -X(3)
      X(7) = -X(2)
      X(8) = -X(1)
      X(9) = -X(0)
      W(0) =  0.066671344308688
      W(1) =  0.149451349150581
      W(2) =  0.219086362515982
      W(3) =  0.269266719309996
      W(4) =  0.295524224714753
      W(5) =  W(4)
      W(6) =  W(3)
      W(7) =  W(2)
      W(8) =  W(1)
      W(9) =  W(0)
      case default
      X = 0.0
      W = 0.0
      end select
      end subroutine GaussRule 

c p=polynomial order
c m=
c U= knot vector
c d=0
c q=p+1 (order for Gauss integration)
c r=number of elements
c O,J,W,X,N output
      subroutine BasisData(p,m,U,d,q,r,O,J,W,X,N)
      implicit none
      integer(kind=4), intent(in)  :: p, m
      real   (kind=8), intent(in)  :: U(0:m)
      integer(kind=4), intent(in)  :: d, q, r
      integer(kind=4), intent(out) :: O(r)
      real   (kind=8), intent(out) :: J(r)
      real   (kind=8), intent(out) :: W(q)
      real   (kind=8), intent(out) :: X(q,r)
      real   (kind=8), intent(out) :: N(0:d,0:p,q,r)

      integer(kind=4) i, iq, ir
      real   (kind=8) uu, Xg(q)
      real   (kind=8) basis(0:p,0:d)

      ir = 1
      do i = p, m-p
         if (U(i) /= U(i+1)) then
            O(ir) = i - p
            ir = ir + 1
         end if
      end do
      call GaussRule(q,Xg,W)
      do ir = 1, r
         i = O(ir) + p
         J(ir) = (U(i+1)-U(i))/2.0
         X(:,ir) = (Xg + 1.0) * J(ir) + U(i)
         do iq = 1, q
            uu = X(iq,ir)
            call DersBasisFuns(i,uu,p,d,U,basis)
            N(:,:,iq,ir) = transpose(basis)
         end do
      end do
      end subroutine BasisData

      subroutine DersBasisFuns(i,uu,p,n,U,ders)
      implicit none
      integer(kind=4), intent(in) :: i, p, n
      real   (kind=8), intent(in) :: uu, U(0:i+p)
      real   (kind=8), intent(out):: ders(0:p,0:n)
      integer(kind=4) :: j, k, r, s1, s2, rk, pk, j1, j2
      real   (kind=8) :: saved, temp, d
      real   (kind=8) :: left(p), right(p)
      real   (kind=8) :: ndu(0:p,0:p), a(0:1,0:p)
      ndu(0,0) = 1.0
      do j = 1, p
         left(j)  = uu - U(i+1-j)
         right(j) = U(i+j) - uu
         saved = 0.0
         do r = 0, j-1
            ndu(j,r) = right(r+1) + left(j-r)
            temp = ndu(r,j-1) / ndu(j,r)
            ndu(r,j) = saved + right(r+1) * temp
            saved = left(j-r) * temp
         end do
         ndu(j,j) = saved
      end do
      ders(:,0) = ndu(:,p)
      do r = 0, p
         s1 = 0; s2 = 1;
         a(0,0) = 1.0
         do k = 1, n
            d = 0.0
            rk = r-k; pk = p-k;
            if (r >= k) then
               a(s2,0) = a(s1,0) / ndu(pk+1,rk)
               d =  a(s2,0) * ndu(rk,pk)
            end if
            if (rk > -1) then
               j1 = 1
            else
               j1 = -rk
            end if
            if (r-1 <= pk) then
               j2 = k-1
            else
               j2 = p-r
            end if
            do j = j1, j2
               a(s2,j) = (a(s1,j) - a(s1,j-1)) / ndu(pk+1,rk+j)
               d =  d + a(s2,j) * ndu(rk+j,pk)
            end do
            if (r <= pk) then
               a(s2,k) = - a(s1,k-1) / ndu(pk+1,r)
               d =  d + a(s2,k) * ndu(r,pk)
            end if
            ders(r,k) = d
            j = s1; s1 = s2; s2 = j;
         end do
      end do
      r = p
      do k = 1, n
         ders(:,k) = ders(:,k) * r
         r = r * (p-k)
      end do
      end subroutine DersBasisFuns

      function FindSpan(n,p,uu,U) result (span)
      implicit none
      integer(kind=4), intent(in) :: n, p
      real   (kind=8), intent(in) :: uu, U(0:n+p+1)
      integer(kind=4)             :: span
      integer(kind=4) low, high
      if (uu >= U(n+1)) then
         span = n
         return
      end if
      if (uu <= U(p)) then
         span = p
         return
      end if
      low  = p
      high = n+1
      span = (low + high) / 2
      do while (uu < U(span) .or. uu >= U(span+1))
         if (uu < U(span)) then
            high = span
         else
            low  = span
         end if
         span = (low + high) / 2
      end do
      end function FindSpan

      function CountSpans(n,p,U) result (nelem)
      implicit none
      integer(kind=4), intent(in) :: n, p
      real   (kind=8), intent(in) :: U(0:n+p+1)
      integer(kind=4) :: i,nelem

      nelem = 0
      i=p
      do while (i.le.n)
         do while ((i.le.n) .and. (U(i) == U(i+1)))
            i = i + 1
         end do
         nelem = nelem + 1
         i = i + 1
      end do
      end function CountSpans

      subroutine ComputeWeights(n,p,nelem,U,W) 
      implicit none
      integer(kind=4), intent(in) :: n, p, nelem
      real   (kind=8), intent(in) :: U(0:n+p+1)
      real   (kind=8), intent(out) :: W(nelem)
      integer(kind=4) :: i,ncont,ne
c ....the weight on edges between elements is equal to p+1, if the continuity is C^(p-1) 
      ne = 0
      i=p
      do while (i.le.n)
         ncont=n*(p-1)+1
         do while ((i.le.n) .and. (U(i) == U(i+1)))
            i = i + 1
            ncont = ncont - n
         end do
         ne = ne + 1
         W(ne)=ncont
         i = i + 1
      end do
      end subroutine ComputeWeights
 
      end module projection_engine

