      module graphics_engine

      use projection_engine

      implicit none

      contains


! -------------------------------------------------------------------
!
! -------------------------------------------------------------------
      subroutine GenericGraphics(vn,f, 
     .  Ux,px,nx,nelemx,resx, 
     .  Uy,py,ny,nelemy,resy, 
     .  Uz,pz,nz,nelemz,resz, 
     .  vis_tool)
      interface
          function f(x, y, z) result (val)
          real (kind=8) :: x,y,z,val
          end function
      end interface

      integer(kind=4), intent(in) :: vn
      integer(kind=4), intent(in) :: nx, px, nelemx, resx
      integer(kind=4), intent(in) :: ny, py, nelemy, resy
      integer(kind=4), intent(in) :: nz, pz, nelemz, resz
      real   (kind=8), intent(in) :: Ux(0:nx+px+1)
      real   (kind=8), intent(in) :: Uy(0:ny+py+1)
      real   (kind=8), intent(in) :: Uz(0:nz+pz+1)
      integer(kind=4), intent(in) :: vis_tool
      integer(kind=4) :: mx,my,mz,ngx,ngy,ngz
      integer(kind=4) :: ex,ey,ez ! iterators
      integer(kind=4) :: Ox(nelemx),Oy(nelemy),Oz(max(1,nelemz))
      real   (kind=8) :: Jx(nelemx),Jy(nelemy),Jz(max(1,nelemz))
      real   (kind=8) :: Wx(px+1),Wy(py+1),Wz(pz+1)
      real   (kind=8) :: Xx(px+1,nelemx)
      real   (kind=8) :: Xy(py+1,nelemy)
      real   (kind=8) :: Xz(pz+1,max(1,nelemz))
      real   (kind=8) :: NNx(0:0,0:px,px+1,nelemx), 
     .                   NNy(0:0,0:py,py+1,nelemy), 
     .                   NNz(0:0,0:pz,pz+1,max(1,nelemz))
      real   (kind=8) :: J,W
      real   (kind=8) :: V(0:nx*resx,0:ny*resy,0:nz*resz) ! volume
      real   (kind=8) :: temp ! result in volume point
      real   (kind=8) :: stepx, stepy, stepz
      mx  = nx + px + 1
      ngx = px + 1
      my  = ny + py + 1
      ngy = py + 1
      mz  = nz + pz + 1
      ngz = pz + 1

      call BasisData(px,mx,Ux,0,ngx,nelemx,Ox,Jx,Wx,Xx,NNx)
      call BasisData(py,my,Uy,0,ngy,nelemy,Oy,Jy,Wy,Xy,NNy)
      call BasisData(pz,mz,Uz,0,ngz,nelemz,Oz,Jz,Wz,Xz,NNz)

      stepx = 1.d0 / (nx*resx)
      stepy = 1.d0 / (ny*resy)
      stepz = 1.d0 / (nz*resz)

      do ex = 0, nx*resx !loops over intervals in x,y,z
         do ey = 0, ny*resy
            do ez = 0, nz*resz
               temp = f(stepx*ex, stepy*ey, stepz*ez)
               V(ex,ey,ez) = temp
            enddo
         enddo
      enddo

      if (vis_tool == 1) then
          call GnuplotPrint(vn,nx*resx,ny*resy,nz*resz,V)
      endif

      end subroutine

! -------------------------------------------------------------------
! vn - visualization number
! vcount - number of components to visualize
! Ux,Uy,Uz - knots vectors
! px,py,pz - orders
! nx,ny,nz - number of intervals (problems size is (nx+1)*(ny+1)*(nz+1)
! nelemx,nelemy,nelemz - number of elements
! resx,resy,resz - resolution of element
! F3 - solution
! vis_tool - visualization tool =1 gnuplot =2 ParaView
! -------------------------------------------------------------------
      subroutine Graphics(vn,vcount, 
     .  Ux,px,nx,nelemx,resx, 
     .  Uy,py,ny,nelemy,resy, 
     .  Uz,pz,nz,nelemz,resz, 
     .  F3,vis_tool)
      integer(kind=4), intent(in) :: vn, vcount
      integer(kind=4), intent(in) :: nx, px, nelemx, resx
      integer(kind=4), intent(in) :: ny, py, nelemy, resy
      integer(kind=4), intent(in) :: nz, pz, nelemz, resz
      real   (kind=8), intent(in) :: Ux(0:nx+px+1)
      real   (kind=8), intent(in) :: Uy(0:ny+py+1)
      real   (kind=8), intent(in) :: Uz(0:nz+pz+1)
      real   (kind=8), intent(in) :: 
     .            F3(0:(nz+1)-1,0:(nx+1)*(ny+1)-1) !,vcount)
      integer(kind=4), intent(in) :: vis_tool
      integer(kind=4) :: mx,my,mz,ngx,ngy,ngz
      integer(kind=4) :: ex,ey,ez ! iterators
      integer(kind=4) :: kx,ky,kz ! iterators
      integer(kind=4) :: ax,ay,az ! iterators
      integer(kind=4) :: Ox(nelemx),Oy(nelemy),Oz(max(1,nelemz))
      real   (kind=8) :: Jx(nelemx),Jy(nelemy),Jz(max(1,nelemz))
      real   (kind=8) :: Wx(px+1),Wy(py+1),Wz(pz+1)
      real   (kind=8) :: Xx(px+1,nelemx)
      real   (kind=8) :: Xy(py+1,nelemy)
      real   (kind=8) :: Xz(pz+1,max(1,nelemz))
      real   (kind=8) :: NNx(0:0,0:px,px+1,nelemx), 
     .                   NNy(0:0,0:py,py+1,nelemy), 
     .                   NNz(0:0,0:pz,pz+1,max(1,nelemz))
      real   (kind=8) :: J,W
      real   (kind=8) :: V(vcount,0:nx*resx,0:ny*resy,0:nz*resz) ! volume
      real   (kind=8) :: temp ! result in volume point
      real   (kind=8) :: stepx, stepy, stepz
      real   (kind=8) :: Pw(0:nx,0:ny,0:nz,vcount) ! solution coefficients
      integer :: d, i
      integer :: lx
      write(*,*)'call Graphics'

! do lx = 0, nx+px+1
!   print *,Ux(lx)
!   print *,Uy(lx)
!   print *,Uz(lx)
! end do
      d = 0
      mx  = nx + px + 1
      ngx = px + 1
      my  = ny + py + 1
      ngy = py + 1
      mz  = nz + pz + 1
      ngz = pz + 1

      call BasisData(px,mx,Ux,d,ngx,nelemx,Ox,Jx,Wx,Xx,NNx)
      call BasisData(py,my,Uy,d,ngy,nelemy,Oy,Jy,Wy,Xy,NNy)
      call BasisData(pz,mz,Uz,d,ngz,nelemz,Oz,Jz,Wz,Xz,NNz)
c      write(*,*) 'After calling BasisData'

      stepx = 1.d0 / (nx*resx)
      stepy = 1.d0 / (ny*resy)
      stepz = 1.d0 / (nz*resz)

c      write(*,*)'Computing coeffs'
      do i=1,vcount !skip it 
c          write(*,*)'For', i
          do ex=0,nx
              do ey=0,ny
                   do ez=0,nz
                      if(nelemz==0)then
                        Pw(ex,ey,ez,i) = 0.d0
                        if(ez.gt.0)cycle
                        Pw(ex,ey,ez,i) = F3(ez,(nx+1)*ey+ex) !,i)
c-> debug
c                        write(*,*)'Pw(',ex,ey,ez,i,') =',Pw(ex,ey,ez,i),
c     .          '<-F3(',ez,(nx+1)*ey+ex,')=',F3(ez,(nx+1)*ey+ex) !,i)
c<- debug
                      else
                        Pw(ex,ey,ez,i) = F3(ez,(nx+1)*ey+ex) !,i3)
                      endif
                  enddo
              enddo
          enddo
      enddo

c      write(*,*)'Computing values'

      do i = 1, vcount
c          write(*,*)'For', i
          do ex = 0, nx*resx !loops over intervals in x,y,z
             do ey = 0, ny*resy
                do ez = 0, nz*resz
                   if(nelemz==0)then
                     if(ez.gt.0)cycle
                     temp=Pw(ex,ey,ez,i)
c                     call VolumePoint2D(d,nx,px,Ux,ny,py,Uy, 
c     .                 nz,pz,Uz,Pw(:,:,:,i),stepx*ex,stepy*ey, 
c     .                 stepz*ez,temp)
                   else
                     temp=Pw(ex,ey,ez,i)
c                     call VolumePoint(d,nx,px,Ux,ny,py,Uy, 
c     .                 nz,pz,Uz,Pw(:,:,:,i),stepx*ex,stepy*ey, 
c     .                 stepz*ez,temp)
                   endif
                   V(i,ex,ey,ez) = temp
                enddo
             enddo
          enddo
      enddo

      if (vis_tool == 1) then
          write(*,*)'call GnuplotPrint'
          call GnuplotPrint(vn,nx*resx,ny*resy,nz*resz,V(1,:,:,:))
      else
          write(*,*)'call ParaViewPrint'
          if(nelemz==0)then
c            write(*,*)V(:,:,:,0)
            d=0
            call ParaViewPrint(vn,vcount,nx*resx,ny*resy,d,V)
          else
            call ParaViewPrint(vn,vcount,nx*resx,ny*resy,nz*resz,V)
          endif
      endif

      end subroutine

! -------------------------------------------------------------------
! (Nathan)
!     
! d = 0
! n - number of intervals
! p - polynomial order
! U - knot vector
! Pw - 
! uu - coordinates
! C - output
! -------------------------------------------------------------------
      subroutine CurvePoint(d,n,p,U,Pw,uu,C)
      integer(kind=4), intent(in)  :: d
      integer(kind=4), intent(in)  :: n, p
      real   (kind=8), intent(in)  :: U(0:n+p+1)
      real   (kind=8), intent(in)  :: Pw(d,0:n)
      real   (kind=8), intent(in)  :: uu
      real   (kind=8), intent(out) :: C(d)
      integer(kind=4) :: j, span
      real   (kind=8) :: basis(0:p)

      span = FindSpan(n,p,uu,U)

      call BasisFuns(span,uu,p,U,basis)

      C = 0
      do j = 0, p
        C = C + basis(j)*Pw(:,span-p+j)
      enddo

      end subroutine


! -------------------------------------------------------------------
! d = 0
! n,m - number of intervals
! p,q - polynomial order
! U,V - knot vector
! Pw - 
! uu,vv - coordinates
! S - output
! -------------------------------------------------------------------
      subroutine SurfacePoint(d,n,p,U,m,q,V,Pw,uu,vv,S)
      integer(kind=4), intent(in)  :: d
      integer(kind=4), intent(in)  :: n, p
      integer(kind=4), intent(in)  :: m, q
      real   (kind=8), intent(in)  :: U(0:n+p+1)
      real   (kind=8), intent(in)  :: V(0:m+q+1)
      real   (kind=8), intent(in)  :: Pw(d,0:m,0:n)
      real   (kind=8), intent(in)  :: uu, vv
      real   (kind=8), intent(out) :: S(d)
      integer(kind=4) :: uj, vj, uspan, vspan
      real   (kind=8) :: ubasis(0:p), vbasis(0:q)

      uspan = FindSpan(n,p,uu,U)
      vspan = FindSpan(m,q,vv,V)

      call BasisFuns(uspan,uu,p,U,ubasis)
      call BasisFuns(vspan,vv,q,V,vbasis)

      S = 0.0
      do uj = 0, p
        do vj = 0, q
          S = S + ubasis(uj)*vbasis(vj)*Pw(:,vspan-q+vj,uspan-p+uj)
        enddo
      enddo

      end subroutine

! -------------------------------------------------------------------
! d = 0
! n,m,o - number of intervals
! p,q,r - polynomial order
! U,V,W - knot vector
! Pw - 
! uu,vv,ww - coordinates
! S - output
! -------------------------------------------------------------------
      subroutine VolumePoint(d,n,p,U,m,q,V,o,r,W,Pw,uu,vv,ww,S)
      integer(kind=4), intent(in)  :: d
      integer(kind=4), intent(in)  :: n, p
      integer(kind=4), intent(in)  :: m, q
      integer(kind=4), intent(in)  :: o, r
      real   (kind=8), intent(in)  :: U(0:n+p+1)
      real   (kind=8), intent(in)  :: V(0:m+q+1)
      real   (kind=8), intent(in)  :: W(0:o+r+1)
      real   (kind=8), intent(in)  :: Pw(0:o,0:m,0:n)
      real   (kind=8), intent(in)  :: uu, vv, ww
      real   (kind=8), intent(out) :: S
      integer(kind=4) :: uj, vj, wj, uspan, vspan, wspan
      real   (kind=8) :: ubasis(0:p), vbasis(0:q), wbasis(0:r)

      uspan = FindSpan(n,p,uu,U) 
      vspan = FindSpan(m,q,vv,V)
      wspan = FindSpan(o,r,ww,W)

      call BasisFuns(uspan,uu,p,U,ubasis)
      call BasisFuns(vspan,vv,q,V,vbasis)
      call BasisFuns(wspan,ww,r,W,wbasis)

      S = 0.0
      do uj = 0, p
        do vj = 0, q
          do wj = 0, r
            S = S + ubasis(uj)*vbasis(vj)*wbasis(wj)
     .         *Pw(wspan-r+wj,vspan-q+vj,uspan-p+uj)
          enddo
        enddo
      enddo

      end subroutine

! -------------------------------------------------------------------
! d = 0
! n,m,o - number of intervals
! p,q,r - polynomial order
! U,V,W - knot vector
! Pw - 
! uu,vv,ww - coordinates
! S - output
! -------------------------------------------------------------------
      subroutine VolumePoint2D(d,n,p,U,m,q,V,o,r,W,Pw,uu,vv,ww,S)
      integer(kind=4), intent(in)  :: d
      integer(kind=4), intent(in)  :: n, p
      integer(kind=4), intent(in)  :: m, q
      integer(kind=4), intent(in)  :: o, r
      real   (kind=8), intent(in)  :: U(0:n+p+1)
      real   (kind=8), intent(in)  :: V(0:m+q+1)
      real   (kind=8), intent(in)  :: W(0:o+r+1)
      real   (kind=8), intent(in)  :: Pw(0:o,0:m,0:n)
      real   (kind=8), intent(in)  :: uu, vv, ww
      real   (kind=8), intent(out) :: S
      integer(kind=4) :: uj, vj, wj, uspan, vspan, wspan, r2D
      real   (kind=8) :: ubasis(0:p), vbasis(0:q), wbasis(0:r)

      uspan = FindSpan(n,p,uu,U) 
      vspan = FindSpan(m,q,vv,V)
      wspan = FindSpan(o,r,ww,W)

      call BasisFuns(uspan,uu,p,U,ubasis)
      call BasisFuns(vspan,vv,q,V,vbasis)
      call BasisFuns(wspan,ww,r,W,wbasis)

      S = 0.0
      do uj = 0, p
        do vj = 0, q
c          do wj = 0, r
            S = S + ubasis(uj)*vbasis(vj)
     .         *Pw(0,vspan-q+vj,uspan-p+uj)
c          enddo
        enddo
      enddo

      end subroutine

    ! -------------------------------------------------------------------
    ! i - function index
    ! uu -coordinates
    ! p - order
    ! U - knot vector
    ! ders - output
    ! -------------------------------------------------------------------
      subroutine BasisFuns(i,uu,p,U,ders)
      integer(kind=4), intent(in) :: i, p
      real   (kind=8), intent(in) :: uu, U(0:i+p)
      real   (kind=8), intent(out):: ders(0:p)
      integer(kind=4) :: d

      d = 0
      call DersBasisFuns(i,uu,p,d,U,ders)

      end subroutine


! -------------------------------------------------------------------
! -> printing for gnuplot
!  
! vn - visualization number
! ln - layer number
! lw - layer width
! lh - layer height
! L - layer   
! -------------------------------------------------------------------
      subroutine GnuplotLayerPrint(vn,ln,lw,lh,L)
      integer   (kind=4), intent(in) :: vn, ln, lw, lh
      real      (kind=8), intent(in) :: L(0:lw, 0:lh)
      integer   (kind=4) :: i, j, output
      character (len=50) :: fn,gname,temp,c

      fn = 'files/gp_step_'
      write(temp,'(I10)') vn
      fn = trim(adjustl(fn))//trim(adjustl(temp))
      fn = trim(adjustl(fn))//trim(adjustl('_layer_'))
      write(temp,'(I10)') ln
      fn = trim(adjustl(fn))//trim(adjustl(temp))
      fn = trim(adjustl(fn))//trim(adjustl('.plot'))
      output = 10
      open(unit=output,file=fn, 
     .  form='formatted',access='sequential',status='unknown')
      write(output,*)'set size'

      write(temp,'(I10)') lw
      temp = trim(adjustl('set xrange [0:'))//trim(adjustl(temp))
      temp = trim(adjustl(temp))//trim(adjustl(']'))
      write(output,*)temp

      write(temp,'(I10)') lh
      temp = trim(adjustl('set yrange [0:'))//trim(adjustl(temp))
      temp = trim(adjustl(temp))//trim(adjustl(']'))
      write(output,*)temp

      write(temp,'(I10)') ln
      temp = trim(adjustl('set title ''layer:'))//trim(adjustl(temp))
      temp = trim(adjustl(temp))//trim(adjustl(''''))
      write(output,*)temp

      write(output,*)'set terminal gif'

      write(temp,'(I10)') vn
      gname = trim(adjustl('set output ''out'))//trim(adjustl(temp))
      gname = trim(adjustl(gname))//trim(adjustl('_'))
      write(temp,'(I10)') ln
      gname = trim(adjustl(gname))//trim(adjustl(temp))
      gname = trim(adjustl(gname))//trim(adjustl('.gif'''))
      write(output,*)gname

      write(output,*)'plot ''-'' with image'
      do i = 0, lh
        do j = 0, lw
          write(temp,'(F30.10)') L(i,j)
          write(output,*)i,' ',j,' ',temp
        enddo
      enddo

      write(output,*)'e'
      close(output)

      end subroutine

! -------------------------------------------------------------------
! vn - visualization number
! vw - volume height
! vh - volume heigth
! vd - volume depth
! V - volume
! -------------------------------------------------------------------
      subroutine GnuplotPrint(vn,vw,vh,vd,V)
      integer (kind=4), intent(in) :: vn, vw,vh, vd
      real    (kind=8), intent(in) :: V(0:vw,0:vh,0:vd)
      integer (kind=4) :: i
      real    (kind=8) :: L(0:vh,0:vd)

      do i = 0, vw
        L = V(i,:,:)
        call GnuplotLayerPrint(vn,i,vh,vd,L)
      enddo

      end subroutine

! -------------------------------------------------------------------
! Write XML that can be load by ParaView through VTK Reader.
! Uses VTK XML DataImage format
!
! vn       - visualization number
! vcount   - number of components
! vw       - volume width
! vh       - volume height
! vd       - volume depth
! V        - volume components
! -------------------------------------------------------------------
      subroutine ParaViewPrint(vn,vcount,vw,vh,vd,V)
      integer   (kind=4), intent(in) :: vn, vcount, vw, vh, vd
      real      (kind=8), intent(in) :: V(vcount,0:vw,0:vh,0:vd)
      integer   (kind=4) :: i, j, k, l
      integer   (kind=4) :: output
      character (len=200) :: fn,temp,extent

      ! open file
      fn = 'files/pv_step_'
      write(temp,'(I10)') vn
      fn = trim(adjustl(fn))//trim(adjustl(temp))
      fn = trim(adjustl(fn))//trim(adjustl('.vti'))
      output = 10
      open(unit=output,file=fn, 
     .  form='formatted',access='sequential',status='unknown')

      ! XML version/root
      write (output,'(A)') '<?xml version="1.0"?>'
      write (output,'(A)') '<VTKFile type="ImageData" version="0.1">'

      ! Prepare extent (count of parts in each dimension for later use
      ! (format "x1 x2 y1 y2 z1 z2"
      write (extent,*) '0'
      write (temp,'(I10)') vw
      extent = trim(adjustl(extent))//' '//trim(adjustl(temp))
      extent = trim(adjustl(extent))//' 0'
      write (temp,'(I10)') vh
      extent = trim(adjustl(extent))//' '//trim(adjustl(temp))
      extent = trim(adjustl(extent))//' 0'
      write (temp,'(I10)') vd
      extent = trim(adjustl(extent))//' '//trim(adjustl(temp))

      ! Init ImageData structure for whole region
      temp = trim(adjustl('<ImageData WholeExtent="'))
     .   //trim(adjustl(extent))
      temp = '  '//trim(adjustl(temp))// 
     .  trim(adjustl('" origin="0 0 0" spacing="1 1 1">'))
      write (output,'(A)')'  '//trim(adjustl(temp))

      ! Region consists of one piece in one file
      write (temp,*) '<Piece Extent="'
      temp = trim(adjustl(temp))//trim(adjustl(extent))
      temp = trim(adjustl(temp))//trim('">')
      write (output,'(A)')'    '//trim(adjustl(temp))

      ! Assign computation results to mesh points
      write (output,'(A)')'      <PointData Scalars="Result">'
      ! Assign array of data to mesh
      write (temp, '(I10)') vcount
      write (extent,*) '           <DataArray Name="Result" 
     .    type="Float32" format="ascii" NumberOfComponents="'
      extent = trim(adjustl(extent))//trim(adjustl(temp))
      write(output,'(A)') trim(adjustl(extent))//trim('">')

      ! Output result values by X, Y and Z axis
      write(*,*)'Printing data'
      do i = 0, vd
        do j = 0, vh
          do k = 0, vw
            do l = 1, vcount
              write(temp,'(F30.10)') V(l,k,j,i)
              write(output,'(A)') '          '//trim(adjustl(temp))
            enddo
          enddo
        enddo
      enddo

      ! Close all XML nodes
      write (output,'(A)') '        </DataArray>'
      write (output,'(A)') '      </PointData>'
      write (output,'(A)') '    </Piece>'
      write (output,'(A)') '  </ImageData>'
      write (output,'(A)') '</VTKFile>'
      close(output)
      write(*,*)'Done!'

      end subroutine

      end module
