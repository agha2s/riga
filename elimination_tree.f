c----------------------------------------------------------------------
c
c   module name        - elimination tree routines
c
c----------------------------------------------------------------------
c
c   computer           - machine independent
c
c   latest revision    - June 09
c
c   purpose            - contains elimination tree data structure 
c                        with some methods working on the 
c                        elimination tree
c
c----------------------------------------------------------------------
      
      module elimination_tree_mod

c      use supernodes_system_mod

      type tree_node
c.......unique id (corresponds to the node number in elimination tree)
        integer :: id
c.......tree node connectivities
        integer :: father
        integer :: nr_sons
        integer, dimension(:),pointer :: sons

c.......tree node stores list of subdomains (blocks)
        integer :: nr_subdomains
        integer, dimension(:), pointer :: subdomains
      end type tree_node

      type tree3
c
        type(tree_node),dimension(:),allocatable :: TREE_NODES
        integer :: NR_TREE_NODES
        integer :: MAX_TREE_NODES
c
        integer :: root
        integer :: new_root
      end type tree3

      type(tree3) :: TREE_INSTANCE
c
      contains
c----------------------------------------------------------------------
c  ...creates new tree
      subroutine create_tree(nr_nodes)
      TREE_INSTANCE%MAX_TREE_NODES=nr_nodes
      allocate(TREE_INSTANCE%TREE_NODES(
     .  TREE_INSTANCE%MAX_TREE_NODES),STAT=i1)
      if(i1/=0)then
        write(*,*)'create_tree: allocation error',i1,nr_nodes
        stop
      endif
      do i=1,nr_nodes
        TREE_INSTANCE%TREE_NODES(i)%id=i
      enddo
      TREE_INSTANCE%NR_TREE_NODES=0
      end subroutine create_tree
c----------------------------------------------------------------------
c  ...creates tree root
      subroutine create_root(subdomains,nr_subdomains)
      integer, dimension(:) ::subdomains
      TREE_INSTANCE%NR_TREE_NODES = 1
      TREE_INSTANCE%TREE_NODES(1)%nr_sons=0
      TREE_INSTANCE%TREE_NODES(1)%father=0
      TREE_INSTANCE%TREE_NODES(1)%nr_subdomains = nr_subdomains
      allocate(TREE_INSTANCE%TREE_NODES(1)%subdomains(nr_subdomains),
     .  STAT=i1)
      if(i1/=0)then
        write(*,*)'create_root: allocation error',i1
        stop
      endif
      TREE_INSTANCE%TREE_NODES(1)%subdomains(1:nr_subdomains) = 
     .   subdomains(1:nr_subdomains)
      TREE_INSTANCE%root = 1
      end subroutine create_root
c----------------------------------------------------------------------
c  ...destroys tree
      subroutine delete_tree
      call delete_node_rec(TREE_INSTANCE%root)
      deallocate(TREE_INSTANCE%TREE_NODES)
      TREE_INSTANCE%NR_TREE_NODES=0
      TREE_INSTANCE%MAX_TREE_NODES=0
      end subroutine delete_tree
c- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
      recursive subroutine delete_node_rec(node)
      integer :: node, iprint
      if(TREE_INSTANCE%TREE_NODES(node)%nr_sons.gt.0)then
        do ison=1,TREE_INSTANCE%TREE_NODES(node)%nr_sons
         if(iprint.eq.1)then
           write(*,*)PRINTRANK,'call delete_node_rec for son',ison
         endif
         call delete_node_rec(TREE_INSTANCE%TREE_NODES(node)%sons(ison))
        enddo
        deallocate(TREE_INSTANCE%TREE_NODES(node)%sons)
      endif
      if(associated(TREE_INSTANCE%TREE_NODES(node)%subdomains))then
        deallocate(TREE_INSTANCE%TREE_NODES(node)%subdomains)
      endif
      nullify(TREE_INSTANCE%TREE_NODES(node)%subdomains)
      end subroutine delete_node_rec
c----------------------------------------------------------------------
      subroutine create_new_son(father,nr_subdomains,subdomains)
c  ...pointer to father node
      integer :: father
c  ...number of subdomains assigned to son node
      integer :: nr_subdomains
c  ...pointer to subdomains list
      integer, dimension(:),allocatable :: subdomains
c.....local data
      integer,dimension(:),pointer :: father_sons
      if(TREE_INSTANCE%TREE_NODES(father)%nr_sons.gt.0)then
        allocate(father_sons(TREE_INSTANCE%TREE_NODES(father)%nr_sons),
     .    STAT=i1)
        if(i1/=0)then
          write(*,*)'create_new_son: allocation error',i1
          stop
        endif
        father_sons(1:TREE_INSTANCE%TREE_NODES(father)%nr_sons)=
     .    TREE_INSTANCE%TREE_NODES(father)%sons(
     .       1:TREE_INSTANCE%TREE_NODES(father)%nr_sons)
        deallocate(TREE_INSTANCE%TREE_NODES(father)%sons)
        allocate(TREE_INSTANCE%TREE_NODES(father)%sons(
     .    TREE_INSTANCE%TREE_NODES(father)%nr_sons+1),STAT=i1)
        if(i1/=0)then
          write(*,*)'create_new_son: allocation error',i1
          stop
        endif
        do ison=1,TREE_INSTANCE%TREE_NODES(father)%nr_sons
           TREE_INSTANCE%TREE_NODES(father)%sons(ison)=father_sons(ison)
        enddo
        deallocate(father_sons)
      else
        allocate(TREE_INSTANCE%TREE_NODES(father)%sons(1),STAT=i1)
        if(i1/=0)then
          write(*,*)'create_new_son: allocation error',i1
          stop
        endif
      endif
c.....add a new node
      TREE_INSTANCE%TREE_NODES(father)%nr_sons = 
     .  TREE_INSTANCE%TREE_NODES(father)%nr_sons+1
      TREE_INSTANCE%NR_TREE_NODES = TREE_INSTANCE%NR_TREE_NODES+1
      TREE_INSTANCE%TREE_NODES(father)%sons(
     .  TREE_INSTANCE%TREE_NODES(father)%nr_sons) = 
     .    TREE_INSTANCE%NR_TREE_NODES
c.....initialize the son node
      TREE_INSTANCE%TREE_NODES(TREE_INSTANCE%NR_TREE_NODES)%nr_sons=0
      TREE_INSTANCE%TREE_NODES(TREE_INSTANCE%NR_TREE_NODES)%father = 
     .  father
      TREE_INSTANCE%TREE_NODES(TREE_INSTANCE%NR_TREE_NODES)
     .  %nr_subdomains=nr_subdomains
      allocate(TREE_INSTANCE%TREE_NODES(
     .  TREE_INSTANCE%NR_TREE_NODES)%subdomains(nr_subdomains),STAT=i1)
      if(i1/=0)then
        write(*,*)'create_new_son: allocation error',i1
        stop
      endif
      TREE_INSTANCE%TREE_NODES(TREE_INSTANCE%NR_TREE_NODES)
     .  %subdomains(1:nr_subdomains) = subdomains(1:nr_subdomains)
      end subroutine create_new_son
c----------------------------------------------------------------------
c  ...write down tree data
      subroutine dumpout_tree
      write(40,*)TREE_INSTANCE%NR_TREE_NODES
      call dumpout_node(TREE_INSTANCE%root)
      end subroutine dumpout_tree
c----------------------------------------------------------------------
c  ...write down node
      recursive subroutine dumpout_node(node)
      type(tree3) :: T
      integer :: node
c      integer, allocatable, dimension(:,:) :: pair_subdomains
      integer, allocatable, dimension(:) :: pair_subdomains
      allocate(pair_subdomains(
     .  TREE_INSTANCE%TREE_NODES(node)%nr_subdomains*2))
      do i=1,
     .  TREE_INSTANCE%TREE_NODES(node)%nr_subdomains
        isub=
     .    TREE_INSTANCE%TREE_NODES(node)%subdomains(i)
        pair_subdomains(2*(i-1)+1)=1
        pair_subdomains(2*i)=isub+1
      enddo
c node id, number of elements, elements (we call them subdomains)
      if(TREE_INSTANCE%TREE_NODES(node)%nr_sons.eq.2)then          
        write(40,*)TREE_INSTANCE%TREE_NODES(node)%id,
     .    TREE_INSTANCE%TREE_NODES(node)%nr_subdomains,
     .     pair_subdomains(1:
     .     TREE_INSTANCE%TREE_NODES(node)%nr_subdomains*2),
     .    TREE_INSTANCE%TREE_NODES(
     .    TREE_INSTANCE%TREE_NODES(node)%sons(1))%id,
     .    TREE_INSTANCE%TREE_NODES(
     .    TREE_INSTANCE%TREE_NODES(node)%sons(2))%id
      else
        write(40,*)TREE_INSTANCE%TREE_NODES(node)%id,
     .    TREE_INSTANCE%TREE_NODES(node)%nr_subdomains,
     .     pair_subdomains(1:
     .     TREE_INSTANCE%TREE_NODES(node)%nr_subdomains*2)
      endif
      deallocate(pair_subdomains)
      do ison=1,TREE_INSTANCE%TREE_NODES(node)%nr_sons
        call dumpout_node(TREE_INSTANCE%TREE_NODES(node)%sons(ison))
      enddo
      end subroutine dumpout_node
c----------------------------------------------------------------------
c  ...write down tree data
      subroutine report_tree
      call report_node(TREE_INSTANCE%root)
      end subroutine report_tree
c----------------------------------------------------------------------
c  ...write down node
      recursive subroutine report_node(node)
      type(tree3) :: T
      integer :: node
      iprint=0
      write(*,*)'--------------------------------------------------'
      write(*,*)'Node',node,'father',
     .  TREE_INSTANCE%TREE_NODES(node)%father
      write(*,*)'nr_subdomains',
     .  TREE_INSTANCE%TREE_NODES(node)%nr_subdomains
      write(*,*)'subdomains',TREE_INSTANCE%TREE_NODES(node)%subdomains(
     .  1:TREE_INSTANCE%TREE_NODES(node)%nr_subdomains)
      write(*,*)'nr_sons',TREE_INSTANCE%TREE_NODES(node)%nr_sons
      do ison=1,TREE_INSTANCE%TREE_NODES(node)%nr_sons
        if(iprint.eq.1)then
          write(*,*)'son:',ison, '-> Node:',
     .    TREE_INSTANCE%TREE_NODES(node)%sons(ison)
        endif
        call report_node(TREE_INSTANCE%TREE_NODES(node)%sons(ison))
      enddo
      call pause
      end subroutine report_node
c----------------------------------------------------------------------
      end module elimination_tree_mod
