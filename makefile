
OBJECTS = projection_engine.o \
          pause.o \
          elimination_tree.o \
          partition_engine.o \
          graphics_engine.o \
          utilities.o \
          main.o

SOURCES = projection_engine.f \
          pause.f \
          elimination_tree.f \
          partition_engine.f \
          graphics_engine.f \
          utilities.f \
          main.f

tree_gen: $(OBJECTS) $(SOURCES)
	ifort -g -O0 -o tree_gen $(OBJECTS) $(LIBRARIES)

clean:
	rm -f *.o *.mod */*.o */*.mod

.f.o:
	ifort -traceback -g -O3 -c $*.f -o $*.o
